import Vue from 'vue'
import axios from 'axios'
import { Notify, Toast } from 'vant'
import Promise from 'es6-promise'

Promise.polyfill()

// axios 配置
axios.defaults.timeout = 10000
axios.defaults.withCredentials = false
axios.defaults.async = true
axios.defaults.crossDomain = true
// // 创建axios 实例
// const service = axios.create({
//   baseURL: process.env.BASE_API, // api的base_url
//   timeout: 10000 // 请求超时时间
// })
let toast
// request 拦截器
axios.interceptors.request.use(
  config => {
    // 这里可以自定义一些config 配置
    toast = Toast.loading({
      duration: 0, // 持续展示 toast
      forbidClick: true, // 禁用背景点击
      loadingType: 'spinner',
      message: '加载中...'
    })
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// response 拦截器
axios.interceptors.response.use(
  response => {
    if (toast) {
      setTimeout(() => {
        Vue.nextTick(() => {
          toast.close()
        })
      }, 300)
    }
    const res = response.data
    // 这里处理一些response 正常放回时的逻辑

    // 比如， 如果code 非 200 统一提示错误，当然你仍可以更详细的区分
    if (res.code !== 200) {
      Notify({
        message: res.msg,
        duration: 3000
      })
    }

    return res
  },
  error => {
    if (toast) {
      setTimeout(() => {
        Vue.nextTick(() => {
          toast.close()
        })
      }, 300)
    }
    Notify({
      message: '通知内容',
      duration: 1000
    })
    // 这里处理一些response 出错时的逻辑
    return Promise.reject(error)
  }
)

export default axios
